
#include "CountdownManager.hpp"

#define CYCLIC_ERASE(v, i) v[i] = v[v.size() - 1]; v.erase(v.end() - 1);

CountdownManager* CountdownManager::ourInstance = nullptr;

void CountdownManager::Invoke(std::function<void()> aFunction, float aValue, bool aShouldLoop)
{
	if (!ourInstance) { return; }

	TimerObject obj;
	obj.myFunction = aFunction;

	obj.myValue = aValue;
	obj.myStartValue = obj.myValue;

	obj.myIsLooping = aShouldLoop;

	myContainer.push_back(obj);
}

void CountdownManager::Devoke(std::function<void()> aFunction)
{
	if (!ourInstance) { return; }

	for (size_t i = 0; i < myContainer.size(); i++)
	{
		if (aFunction.target_type() == myContainer[i].myFunction.target_type())
		{
			CYCLIC_ERASE(myContainer, i);
			return;
		}
	}
}

CountdownManager* CountdownManager::GetInstance()
{
	return ourInstance;
}

void CountdownManager::Create()
{
	if (!ourInstance) ourInstance = new CountdownManager();
}

void CountdownManager::Destroy()
{
	ourInstance = nullptr;
}

void CountdownManager::Update(float aDeltaTime)
{
	for (int i = myContainer.size() - 1; i >= 0; i--)
	{
		myContainer[i].myValue -= aDeltaTime;

		if (myContainer[i].myValue <= 0)
		{
			myContainer[i].myFunction();


			if (!myContainer[i].myIsLooping)
			{
				CYCLIC_ERASE(myContainer, i);
			}
			else
			{
				myContainer[i].myValue = myContainer[i].myStartValue;
			}
		}
	}
}
