
#pragma once
#include <functional>
#include <vector>

#define CDM CountdownManager

class CountdownManager
{
public:
	static void Create();
	static void Destroy();
	static CountdownManager* GetInstance();

	//aFunction kallas efter aValue sekunder, aShouldLoop �r valfritt och g�r att timern k�r om
	void Invoke(std::function<void()> aFunction, float aValue, bool aShouldLoop = false);

	//Stoppar loopande funktion
	void Devoke(std::function<void()> aFunction);

	//Uppdaterar timern
	void Update(float aDeltaTime);

private:
	CountdownManager() = default;
	~CountdownManager() = default;
	
	static CountdownManager* ourInstance;

	struct TimerObject
	{
		std::function<void()> myFunction;
		float myValue;
		float myStartValue;
		bool myIsLooping = false;
	};

	std::vector<TimerObject> myContainer;
};

