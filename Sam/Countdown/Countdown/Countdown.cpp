
#include <iostream>
#include "CountdownManager.hpp"

void Test()
{
	std::cout << "Test k�rdes!" << std::endl;
}
void Test2()
{
	std::cout << "Test2 k�rdes!" << std::endl;
}
void Test3()
{
	std::cout << "Test3 k�rdes! Denna ska loopas" << std::endl;
}
void Test4()
{
	std::cout << "Test4 k�rdes!" << std::endl;
}

int main()
{
	CDM::Create();

	CDM::GetInstance()->Invoke(&Test, 3);
	CDM::GetInstance()->Invoke(&Test2, 8);
	CDM::GetInstance()->Invoke(&Test3, 1, true);
	CDM::GetInstance()->Invoke(&Test4, 15);

	while (true)
	{
		CDM::GetInstance()->Update(0.00001f);
	}

}

