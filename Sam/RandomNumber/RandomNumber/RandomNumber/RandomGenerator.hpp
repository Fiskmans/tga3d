
#pragma once
#include <random>
#include <numeric>
#include <assert.h>

#define Random RandomGenerator

class RandomGenerator
{
public:

	template<class T> static T* Either(T* a, T* b);
	static int Between(int aMin, int aMax);
	static float Between(float aMin, float aMax);
	
	template<class T> static T Weighted(const std::vector<T>& aValues, const std::vector<float>& aWeights);

	static void Create();
	static void Destroy();

	static RandomGenerator* GetInstance();

private:
	RandomGenerator() = default;
	~RandomGenerator() = default;

	static RandomGenerator* ourInstance;
};

//Get a or b
template<class T> T* RandomGenerator::Either(T* a, T* b)
{
	std::random_device crypto_random_generator;
	std::uniform_int_distribution<int> int_distribution(0, 1);

	int actual_distribution[2] = { 0, 0 };
	int result = 0;

	for (int i = 0; i < 10000; i++)
	{
		result = int_distribution(crypto_random_generator);
		actual_distribution[result]++;
	}

	if (actual_distribution[0] > actual_distribution[1]) { return a; } return b;
}

//Get random with weight factor
template<class T> inline T RandomGenerator::Weighted(const std::vector<T>& aValues, const std::vector<float>& aWeights)
{
	assert(aValues.size() == aWeights.size() && "Varje v�rde har inte en vikt");

	float totalWeight = std::accumulate(aWeights.begin(), aWeights.end(), 0);

	int randomNumber = Between(0.f, totalWeight);

	for (size_t i = 0; i < aValues.size(); i++)
	{
		if (randomNumber < aWeights[i])
		{
			return aValues[i];
		}

		randomNumber -= aWeights[i];
	}

	//Hit ner ska du ej kunna komma
	return T();
}
