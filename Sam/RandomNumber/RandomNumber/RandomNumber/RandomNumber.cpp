
#include "stdafx.h"
#include "RandomGenerator.hpp"
#include <iostream>
#include <string>
#include <vector>

int main()
{
	Random::Create();

	std::vector<int> g = { 4, 3, 9, 12, 6, 55 };
	std::vector<float> w = { 2, 1, 1, 5, 9, 13 };

	for (size_t i = 0; i < 1000; i++)
	{
		int nice = Random::GetInstance()->Weighted<int>(g, w);
		std::cout << nice << std::endl;
	}


    return 0;
}

