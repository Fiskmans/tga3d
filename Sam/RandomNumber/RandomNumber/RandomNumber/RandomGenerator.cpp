#include "stdafx.h"
#include "RandomGenerator.hpp"

RandomGenerator* RandomGenerator::ourInstance = nullptr;

void RandomGenerator::Create()
{
	if (!ourInstance) ourInstance = new RandomGenerator();
}

void RandomGenerator::Destroy()
{
	ourInstance = nullptr;
}

RandomGenerator* RandomGenerator::GetInstance()
{
	return ourInstance;
}

//Get value between aMin and aMax
int RandomGenerator::Between(int aMin, int aMax)
{
	if (aMin > aMax) { int val = aMax; aMax = aMin; aMin = val; }

	std::random_device crypto_random_generator;
	std::uniform_int_distribution<int> int_distribution(aMin, aMax);

	int* actual_distribution = new int[aMax];
	memset(actual_distribution, 0, sizeof(actual_distribution));
	int result = 0;

	for (int i = 0; i < 10000; i++)
	{
		result = int_distribution(crypto_random_generator);
		actual_distribution[result]++;
	}

	return result;
}

////Get value between aMin and aMax
float RandomGenerator::Between(float aMin, float aMax)
{
	if (aMin > aMax) { float val = aMax; aMax = aMin; aMin = val; }

	std::random_device crypto_random_generator;
	std::uniform_int_distribution<int> int_distribution(aMin, aMax);

	int* actual_distribution = new int[aMax];
	memset(actual_distribution, 0, sizeof(actual_distribution));
	int result = 0;

	for (int i = 0; i < 10000; i++)
	{
		result = int_distribution(crypto_random_generator);
		actual_distribution[result]++;
	}

	return result;
}

