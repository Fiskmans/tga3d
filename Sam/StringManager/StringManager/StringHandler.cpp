
#include "StringHandler.hpp"

std::string StringHandler::ToUpper(std::string aString)
{
	std::string toReturn = "";

	for (size_t i = 0; i < aString.size(); i++)
	{
		if (aString[i] >= 97 && aString[i] <= 122)
		{
			toReturn += aString[i] - 32;
		}
		else
		{
			toReturn += aString[i];
		}
	}

	return toReturn;
}

std::string StringHandler::ToLower(std::string aString)
{
	std::string toReturn = "";

	for (size_t i = 0; i < aString.size(); i++)
	{
		if (aString[i] >= 33 && aString[i] <= 96)
		{
			toReturn += aString[i] + 32;
		}
		else
		{
			toReturn += aString[i];
		}

	}

	return toReturn;
}
