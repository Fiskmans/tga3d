
#pragma once
#include <string>

#define SH StringHandler

class StringHandler
{
public:
	static std::string ToUpper(std::string aString);
	static std::string ToLower(std::string aString);


	StringHandler() = delete;
	~StringHandler() = delete;
};

