#include "Logger.h"
#include <iostream>


namespace Logger
{
	Type Filter = Type::All;

	void Log(Type aType, const std::string& aMessage)
	{
		if ((aType & Filter) != 0)
		{
			std::cout << aMessage << std::endl;
		}
	}

	void Log(Type aType, const std::wstring& aMessage)
	{
		std::cout << "wstring are not supported" << std::endl;
	}
	void SetFilter(Type aFilter)
	{
		Filter = aFilter;
	}
}

