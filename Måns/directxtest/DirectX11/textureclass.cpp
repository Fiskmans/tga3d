#include "textureclass.h"
#include "Logger.h"

FileWatcher TextureClass::ourFileWatcher;

TextureClass::TextureClass()
{
	m_texture = 0;
}


TextureClass::TextureClass(const TextureClass& other)
{
}


TextureClass::~TextureClass()
{
	if (!ourFileWatcher.UnRegister(myFileHandle))
	{
		Logger::Log(Logger::Type::SystemError, "FileWatcher failed to unregister: " + myFileHandle);
	}
}

bool TextureClass::Initialize(ID3D11Device* device,const CHAR* filename)
{
	HRESULT result;

	myFileHandle = ourFileWatcher.RegisterCallback(filename, std::bind(&TextureClass::ReloadTexture, this, device, std::placeholders::_1), false);

	// Load the texture in.
	result = D3DX11CreateShaderResourceViewFromFile(device, filename, NULL, NULL, &m_texture, NULL);
	if (FAILED(result))
	{
		return false;
	}

	return true;
}

void TextureClass::Shutdown()
{
	// Release the texture resource.
	if (m_texture)
	{
		m_texture->Release();
		m_texture = nullptr;
	}

	return;
}

ID3D11ShaderResourceView* TextureClass::GetTexture()
{
	return m_texture;
}

void TextureClass::FlushChanges()
{
	ourFileWatcher.FlushChanges();
}

void TextureClass::ReloadTexture(ID3D11Device* aDevice, const std::string& aFileName)
{
	Logger::Log(Logger::Type::SystemInfo, "Reloading texture: " + aFileName);
	Shutdown();
	if (!Initialize(aDevice,aFileName.c_str()))
	{
		Logger::Log(Logger::Type::SystemError, "Reloading of texture: " + aFileName + " Failed");
	}
}
