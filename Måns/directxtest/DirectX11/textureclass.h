#pragma once
//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx11tex.h>
#include <FileWatcher.h>

////////////////////////////////////////////////////////////////////////////////
// Class name: TextureClass
////////////////////////////////////////////////////////////////////////////////
class TextureClass
{
public:
	TextureClass();
	TextureClass(const TextureClass&);
	~TextureClass();
	bool Initialize(ID3D11Device*,const CHAR*);
	void Shutdown();
	ID3D11ShaderResourceView* GetTexture();
	static void FlushChanges();
private:
	void ReloadTexture(ID3D11Device*,const std::string&);

private:
	static FileWatcher ourFileWatcher;
	ID3D11ShaderResourceView* m_texture;

	FileWatcher::UniqueID myFileHandle;
};