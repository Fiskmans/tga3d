#pragma once
#include <string>


namespace Logger
{
	enum Type : long long
	{
		None = 0LL,
		All = ~0LL,
		SystemInfo = 1LL << 0,
		SystemError = 1LL << 1
	};

	void Log(Type aType, const std::string& aMessage);
	void Log(Type aType, const std::wstring& aMessage);
	void SetFilter(Type aFilter);
}