#pragma once
//////////////
// INCLUDES //
//////////////
#include <d3d11.h>
#include <d3dx10math.h>
#include <d3dx11async.h>
#include <fstream>
#include <FileWatcher.h>
using namespace std;


////////////////////////////////////////////////////////////////////////////////
// Class name: TextureShaderClass
////////////////////////////////////////////////////////////////////////////////
class TextureShaderClass
{
private:
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

public:
	TextureShaderClass();
	TextureShaderClass(const TextureShaderClass&);
	~TextureShaderClass();

	bool Initialize(ID3D11Device*, HWND);
	void Shutdown();
	bool Render(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);

	static void FlushChanges();
private:
	bool InitializeShader(ID3D11Device*, HWND,const CHAR*,const CHAR*);

	bool InitializePixelShader(ID3D11Device*, HWND, const CHAR*);
	bool InitializeVertexShader(ID3D11Device*, HWND, const CHAR*, ID3D10Blob*& aVertexShaderOutput);

	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*, HWND,const CHAR*);

	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);
	void RenderShader(ID3D11DeviceContext*, int);

	void ReloadVertexShader(ID3D11Device* device, HWND aHWND, const std::string& aFile);

private:
	static FileWatcher ourFileWatcher;
	FileWatcher::UniqueID myVertexFileHandle;
	FileWatcher::UniqueID myPixelFileHandle;

	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;
};