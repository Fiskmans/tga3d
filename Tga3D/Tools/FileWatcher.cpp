#include "FileWatcher.h"


#ifdef WIN32
#define stat _stat
#endif
#include "Macros.hpp"



FileWatcher::FileWatcher() : myWorkHorse(std::bind(&FileWatcher::CheckFiles,this))
{
}


FileWatcher::~FileWatcher()
{
	myIsRunning = false;
	myWorkHorse.join();
}

FileWatcher::UniqueID FileWatcher::RegisterCallback(std::string aFile, std::function<CallbackFunction> aCallback, bool aCallImmediately)
{
	StartLock();
	if (aCallImmediately)
	{
		myThreadSafeMap[aFile] = aCallback;
	}
	else
	{
		myCallbackMap[aFile] = aCallback;
	}
	EndLock();

	return UniqueID(aFile);
}

FileWatcher::UniqueID FileWatcher::RegisterFile(std::string aFile)
{
	StartLock();
	mySelfHandleMap.push_back(aFile);
	EndLock();

	return UniqueID(aFile);
}

bool FileWatcher::GetChangedFile(std::string& aOutFile)
{
	if (myHandOver)
	{
		aOutFile = *myHandOver;
		myHandOver = nullptr;
		return true;
	}
	return false;
}

void FileWatcher::FlushChanges()
{
	if (myFunctionHandOver)
	{
		(*myFunctionHandOver)();
		myFunctionHandOver = nullptr;
	}
}

bool FileWatcher::UnRegister(UniqueID aID)
{
	StartLock();
	if (myCallbackMap.count(aID) != 0)
	{
		myCallbackMap.erase(aID);
		EndLock();
		return true;
	}
	if (myThreadSafeMap.count(aID) != 0)
	{
		myThreadSafeMap.erase(aID);
		EndLock();
		return true;
	}
	for (size_t i = 0; i < mySelfHandleMap.size(); i++)
	{
		if (std::string(mySelfHandleMap[i]) == aID)
		{
			mySelfHandleMap.erase(mySelfHandleMap.begin() + i);
			EndLock();
			return true;
		}
	}
	EndLock();
	return false;
}

void FileWatcher::UnRegisterAll()
{
	StartLock();
	myCallbackMap.clear();
	myThreadSafeMap.clear();
	mySelfHandleMap.clear();
	EndLock();
}

void FileWatcher::CheckFiles()
{
	NAMETHREAD(L"FILEWATCHER-Workhorse")
	std::function<void()> holder;
	while (myIsRunning)
	{
	#pragma region Locking
		if (myHasPendingAdd)
		{
			myIsPaused = true;
			while (myIsPaused)
			{
				std::this_thread::yield();
			}
		}
	#pragma endregion

		for (auto& i : myThreadSafeMap)
		{
			if (i.first.CheckChanged())
			{
				i.second(i.first);
			}
		}
		for (auto& i : myCallbackMap)
		{
			if (!myFunctionHandOver && i.first.CheckChanged())
			{
				holder = std::bind(i.second, i.first);
				myFunctionHandOver = &holder;
			}
		}
		for (auto& i : mySelfHandleMap)
		{
			if (&myHandOver && i.CheckChanged())
			{
				myHandOver = &i;
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(RefreshRate));
	}
}

void FileWatcher::StartLock()
{
	myHasPendingAdd = true;
	while (!myIsPaused)
	{
		std::this_thread::yield();
	}
}

void FileWatcher::EndLock()
{
	myHasPendingAdd = false;
	myIsPaused = false;
}

bool FileHandle::CheckChanged() const
{
	struct _stat result;
	if (_stat(myFileName.c_str(), &result) == 0)
	{
		time_t mod_time = result.st_mtime;
		if (mod_time > myLastChangedContainer)
		{
			*const_cast<time_t*>(&myLastChangedContainer) = mod_time;	//const to allow whole object to be used as a key in a std::map, 
																		//changing time does not affect the hash of it so this is safe
			return true;
		}
	}
	return false;
}
