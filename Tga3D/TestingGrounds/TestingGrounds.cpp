// TestingGrounds.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include "..//Tools/FileWatcher.h"
#pragma comment(lib,"Tools.lib")


void Callback(const std::string& aFile)
{
	std::cout << "  func FileChanged: " << aFile << std::endl;
}


void ThreadSafeCallback(const std::string& aFile)
{
	std::cout << "thfunc FileChanged: " << aFile << std::endl;
}

int main()
{
	int a = 0;
	{
		{
			FileWatcher ft;
		}
	}


    std::cout << "Hello World!\n";
	FileWatcher fw;

	fw.RegisterFile("test.txt");
	fw.RegisterFile("test.txt");
	fw.RegisterCallback("test.txt", Callback, false);
	fw.RegisterCallback("test.txt", Callback, false);
	fw.RegisterCallback("test.txt", ThreadSafeCallback, true);

	fw.RegisterCallback("test2.txt", Callback, false);
	fw.RegisterCallback("test2.txt", ThreadSafeCallback, true);

	fw.RegisterCallback("test3.txt", ThreadSafeCallback, true);


	std::string file;
	while (true)
	{
		if (fw.GetChangedFile(file))
		{
			std::cout << "cofunc FileChanged: " << file << std::endl;
		}

		fw.FlushChanges();

	}
}