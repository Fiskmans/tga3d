#pragma once
#include <functional>
#include <vector>
#include <unordered_map>

class StateMachine;
class State
{
public:
	friend class StateMachine;
	State();
	State(StateMachine* aStateMachine);
	virtual ~State();


	void SetCondition(std::function<bool()> aCondition, State* aOtherState);

protected:
	virtual void OnEnter() = 0;
	virtual void OnExit() = 0;
	virtual void Update(float aDeltaTime) = 0;
	void SwitchState();
	std::unordered_map<State*, std::function<bool()>> aStateMap;
	StateMachine* myStateMachine;
private:
};

