#pragma once
#include "State.hpp"
class StateMachine
{
public:
	StateMachine();
	~StateMachine();
	void Start(State* aStartState);
	void SwitchState(State* aOtherState);
	void Update(float aDeltaTime);
	const State* GetCurrentStatePtr() const;
private:

	State* myCurrentState;
};

