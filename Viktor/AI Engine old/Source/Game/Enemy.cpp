#include "stdafx.h"
#include "Enemy.hpp"


Enemy::Enemy(AI::Kinematic& aTarget) :
	myPursue(&myKinematic, &aTarget, 0.4f, 2.f),
	myFace(&myKinematic, &aTarget, 2.f, 3.f),
	myWander(&myKinematic, &aTarget),
	mySeek(&myKinematic, &aTarget, 1.f),
	myAlign(&myKinematic, &aTarget, 1.f, 4.f, 0.01f, 0.6f),
	myArrive(&myKinematic, &aTarget, 1.f, 0.5f, 0.01f, 0.2f)
{
}


Enemy::~Enemy()
{
	delete mySprite;
	mySprite = nullptr;
}

void Enemy::Init()
{
	mySprite = new Tga2D::CSprite("sprites/tga_logo.dds");
	mySprite->SetPivot({ 0.5f, 0.5f });
	mySprite->SetSizeRelativeToImage({ 0.2f, 0.2f });

	myKinematic.myPosition = { 0.4f,0.5f,0 };
	myKinematic.myRotation = 0;
	myKinematic.myVelocity = { 0,0,0 };
}

void Enemy::Update(const float& aDeltaTime)
{
	// Pursue
	//myKinematic.Update(myPursue.GetSteering(), 0.6f, aDeltaTime);
	
	// Face
	//myKinematic.Update(myFace.GetSteering(), 2, aDeltaTime);

	// Wander
	//myKinematic.Update(myWander.GetSteering(), 0.1f, aDeltaTime);

	// Seek
	//myKinematic.Update(mySeek.GetSteering(), 0.5f, aDeltaTime);

	// Align
	//myKinematic.Update(myAlign.GetSteering(), 2.f, aDeltaTime);

	// Arrive
	myKinematic.Update(myArrive.GetSteering(), 2.f, aDeltaTime);

	mySprite->SetPosition({ myKinematic.myPosition.x, myKinematic.myPosition.y });
	mySprite->SetRotation(myKinematic.myOrientation);
	mySprite->Render();
}
