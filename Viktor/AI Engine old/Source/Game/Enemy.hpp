#pragma once
#include "tga2d/sprite/sprite.h"
#include "../AIEngine/Movement/Pursue.hpp"
#include "../AIEngine/Rotation/Face.hpp"
#include "../AIEngine/Movement/Wander.hpp"
#include "..//AIEngine/Movement/Arrive.hpp"
namespace AI {
	class Kinematic;
}

class Enemy
{
public:
	Enemy(AI::Kinematic& aTarget);
	~Enemy();
	void Init();
	void Update(const float& aDeltaTime);
private:
	AI::Kinematic myKinematic;
	AI::Kinematic myTarget;

	AI::Pursue myPursue;
	AI::Face myFace;
	AI::Wander myWander;
	AI::Seek mySeek;
	AI::Align myAlign;
	AI::Arrive myArrive;
	Tga2D::CSprite* mySprite = nullptr;
};

