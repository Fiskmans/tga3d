#pragma once
#include <vector>
#include "../AIEngine/Kinematic.h"
#include "Enemy.hpp"
namespace Tga2D
{
	class CSprite;
}

class CGameWorld
{
public:
	CGameWorld(); 
	~CGameWorld();

	void Init();
	void MoveTarget(float aTimeDelta);
	void Update(float aTimeDelta); 
	Tga2D::CSprite* myTga2dLogoSprite;
	Tga2D::CSprite* myTargetSprite;
private:
	AI::Static myCharacter;
	AI::Static myTarget;
	AI::Kinematic myKin;
	AI::Kinematic myKintarget;
	Enemy myEnemy;
	float timer = 0.0f;
};