#include "stdafx.h"
#include "GameWorld.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/error/error_manager.h>
#include <random>
#include <ctime>
#include "../AIEngine/Tools.h"
#include "../AIEngine/Movement/Arrive.hpp"

CGameWorld::CGameWorld(): myTargetSprite(nullptr), myEnemy(myKintarget)
{
	myTga2dLogoSprite = nullptr;
}


CGameWorld::~CGameWorld()
{
	delete myTga2dLogoSprite;
	myTga2dLogoSprite = nullptr;
	delete myTargetSprite;
	myTargetSprite = nullptr;
}

void CGameWorld::Init()
{
	myTga2dLogoSprite = new Tga2D::CSprite("sprites/tga_logo.dds");
	myTga2dLogoSprite->SetPivot({ 0.5f, 0.5f });
	myTga2dLogoSprite->SetPosition({ 0.0f, 0.0f });
	myTga2dLogoSprite->SetSizeRelativeToImage({ 0.2f, 0.2f });


	myCharacter.myPosition = { 0.0f,0.0f,0 };
	myCharacter.myOrientation = 0;

	myTarget.myPosition = { 0.5,0.5,0 };

	myKin.myPosition = { 0.0f, 0.0f, 0 };
	myKintarget.myPosition = { 0.5, 0.5, 0 };
	myTargetSprite = new Tga2D::CSprite("sprites/tga_logo.dds");
	myTargetSprite->SetPivot({ 0.5f, 0.5f });
	myTargetSprite->SetPosition({ myTarget.myPosition.x, myTarget.myPosition.y });
	myTargetSprite->SetSizeRelativeToImage({ 0.2f, 0.2f });
	myTargetSprite->SetColor({ 1,0,0,1 });
	myEnemy.Init();

	srand(static_cast<unsigned>(time(0)));
}


void CGameWorld::MoveTarget(float aTimeDelta)
{
	if(GetAsyncKeyState('A'))
	{
		myKintarget.myPosition.x -= 0.2f * aTimeDelta;
	}
	if (GetAsyncKeyState('D'))
	{
		myKintarget.myPosition.x += 0.2f * aTimeDelta;
	}
	if (GetAsyncKeyState('W'))
	{
		myKintarget.myPosition.y -= 0.2f * aTimeDelta;
	}
	if (GetAsyncKeyState('S'))
	{
		myKintarget.myPosition.y += 0.2f * aTimeDelta;
	}
	if(GetAsyncKeyState('Q'))
	{
		myKintarget.myOrientation -= 2.f * aTimeDelta;
	}
	if(GetAsyncKeyState('E'))
	{
		myKintarget.myOrientation += 2.f * aTimeDelta;
	}
}

void CGameWorld::Update(float aTimeDelta)
{
	timer -= aTimeDelta;

	if (GetAsyncKeyState(VK_ESCAPE))
	{
		Tga2D::CEngine::Shutdown();
	}

	//myTga2dLogoSprite->SetPosition({ myKin.myPosition.x, myKin.myPosition.y });
	//myTga2dLogoSprite->SetRotation(myKin.myOrientation);

	//myTga2dLogoSprite->Render();

	myEnemy.Update(aTimeDelta);

	MoveTarget(aTimeDelta);

	myTargetSprite->SetPosition({ myKintarget.myPosition.x, myKintarget.myPosition.y });
	myTargetSprite->SetRotation(myKintarget.myOrientation);

	myTargetSprite->Render();
}
