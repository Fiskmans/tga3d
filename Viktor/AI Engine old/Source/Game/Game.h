#pragma once
#include <Windows.h>
#include <fstream>

namespace Tga2D
{
	class CSprite;
}
class CGameWorld;
class CGame
{
public:
	CGame();
	~CGame();

	bool Init(const std::wstring& aVersion = L"", HWND aHWND = nullptr);
private:
	void UpdateCallBack();
	void InitCallBack();
	LRESULT WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	CGameWorld* myGameWorld;
};
