#include "stdafx.h"
#include "Steering.hpp"


SteeringOutput AI::Steering::Arrive(const Kinematic& aCharacter, const Static& aTarget, const float& aMaxAcceleration,
	const float& aMaxSpeed, const float& aTargetRdius, const float& aSlowRadius, const float& aTimeToTarget)
{
	SteeringOutput steering;

	const CommonUtilities::Vector3f direction = aTarget.myPosition - aCharacter.myPosition;
	const float distance = direction.Length();

	if (distance < aTargetRdius)
		return {};

	float targetSpeed;

	if (distance > aSlowRadius)
		targetSpeed = aMaxSpeed;
	else
		targetSpeed = aMaxSpeed * distance / aSlowRadius;

	CommonUtilities::Vector3f targetVelocity = direction;
	targetVelocity.Normalize();
	targetVelocity *= targetSpeed;

	steering.myLinear = targetVelocity - aCharacter.myVelocity;
	steering.myLinear /= aTimeToTarget;

	if (steering.myLinear.Length() > aMaxAcceleration)
	{
		steering.myLinear.Normalize();
		steering.myLinear *= aMaxAcceleration;
	}
	steering.myAngular = 0;

	return steering;
}
