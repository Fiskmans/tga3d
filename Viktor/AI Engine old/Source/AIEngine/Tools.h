#pragma once
#include "Vector/Vector3.hpp"
#include "Vector/Vector2.hpp"

namespace Tools
{
	AI::CU::Vector2f FloatToVector2(const float& aFloat);
	AI::CU::Vector3f FloatToVector3(const float& aFloat);
	float RandomBinomial();
	float MapToRange(float aFloat);
}

