#pragma once
#include "../Rotation/Face.hpp"

namespace AI
{
	class Wander :
		public Face
	{
	public:

		Wander(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAngularAcceleration = 2.f,
			const float aMaxRotation = 3.5f, const float aTargetRdius = 0.05f, const float aSlowRadius = 0.1f,
			const float aWanderOffset = 0.1f, const float aWanderRadius = 0.2f, const float aWanderRate = 0.3f, const float aMaxAcceleration = 0.2f)
			: Face(aCharacter, aTarget, aMaxAngularAcceleration, aMaxRotation, aTargetRdius, aSlowRadius),
			  myWanderOffset(aWanderOffset),
			  myWanderRadius(aWanderRadius),
			  myWanderRate(aWanderRate),
			  myWanderOrientation(aCharacter->myOrientation),
			  myMaxAcceleration(aMaxAcceleration),
			myKinematicPtr(aTarget)
		{
		}

		SteeringOutput GetSteering() override;
		void SetTarget(const Kinematic* aTarget);
	private:

		float myWanderOffset;
		float myWanderRadius;
		float myWanderRate;
		float myWanderOrientation;
		float myMaxAcceleration;
		const Kinematic* myKinematicPtr;
	};
}

