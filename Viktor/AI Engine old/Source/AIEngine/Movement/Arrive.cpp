//#include "pch.h"
#include "Arrive.hpp"
#include "../SteeringOutput.hpp"
#include "../Kinematic.h"

AI::Arrive::Arrive(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration,
	const float aMaxSpeed, const float aTargetRadius, const float aSlowRadius)
	:  SteeringBehaviour(aCharacter, aTarget), 
	myMaxAcceleration(aMaxAcceleration), myMaxSpeed(aMaxSpeed),
	myTargetRadius(aTargetRadius),		 mySlowRadius(aSlowRadius),
	myTimeToTarget(0.1f)
{
}

AI::SteeringOutput AI::Arrive::GetSteering()
{
	SteeringOutput steering;

	const AI::CU::Vector3f direction = myTarget.myPosition - myCharacter->myPosition;
	const float distance = direction.Length();

	myTarget = *myTargetPtr;
	if (distance < myTargetRadius)
		return {};

	float targetSpeed;

	if (distance > mySlowRadius)
		targetSpeed = myMaxSpeed;
	else
		targetSpeed = myMaxSpeed * distance / mySlowRadius;

	AI::CU::Vector3f targetVelocity = direction;
	targetVelocity.Normalize();
	targetVelocity *= targetSpeed;

	steering.myLinear = targetVelocity - myCharacter->myVelocity;
	steering.myLinear /= myTimeToTarget;

	if(steering.myLinear.Length() > myMaxAcceleration)
	{
		steering.myLinear.Normalize();
		steering.myLinear *= myMaxAcceleration;
	}
	steering.myAngular = 0;

	return steering;
}
