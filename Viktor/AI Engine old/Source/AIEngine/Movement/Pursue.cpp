//#include "pch.h"
#include "Pursue.hpp"
#include "../SteeringOutput.hpp"

AI::SteeringOutput AI::Pursue::GetSteering()
{
	myInternalTarget = *myInternalTargetPtr;
	const AI::CU::Vector3f direction = myInternalTarget.myPosition - myCharacter->myPosition;
	const float distance = direction.Length();

	const float speed = myCharacter->myVelocity.Length();

	float prediction;
	if (speed <= distance / myMaxPrediction)
		prediction = myMaxPrediction;
	else
		prediction = distance / speed;

	myInternalTarget.myPosition += myInternalTarget.myVelocity * prediction;

	return Seek::GetSteering();
}
