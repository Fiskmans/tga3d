//#include "pch.h"
#include "Wander.hpp"
#include "../SteeringOutput.hpp"
#include "../Tools.h"
#include "../Rotation/Face.hpp"

AI::SteeringOutput AI::Wander::GetSteering()
{
	myWanderOrientation += Tools::RandomBinomial() * myWanderRate;

	const float targetOrientation = myWanderOrientation + myCharacter->myOrientation;

	myInternalTarget = *myKinematicPtr;
	myInternalTarget.myPosition = myInternalTarget.myPosition + myWanderOffset * Tools::FloatToVector3(myCharacter->myOrientation);
	myInternalTarget.myPosition += myWanderRadius * Tools::FloatToVector3(targetOrientation);

	SteeringOutput steering = Face::GetSteering();
	steering.myLinear = myMaxAcceleration * Tools::FloatToVector3(myCharacter->myOrientation);

	return steering;
}

void AI::Wander::SetTarget(const Kinematic* aTarget)
{
	myInternalTarget = *aTarget;
	//myTargetPtr = aTarget;
	//myInternalTargetPtr = aTarget;
}
