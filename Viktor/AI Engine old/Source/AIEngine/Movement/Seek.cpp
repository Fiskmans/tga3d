//#include "pch.h"
#include "Seek.hpp"
#include "../SteeringOutput.hpp"

AI::Seek::Seek(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration)
	: SteeringBehaviour(aCharacter, aTarget), myMaxAcceleration(aMaxAcceleration)
{
}

AI::SteeringOutput AI::Seek::GetSteering()
{
	SteeringOutput steering;

	steering.myLinear = myTargetPtr->myPosition - myCharacter->myPosition;

	steering.myLinear.Normalize();
	steering.myLinear *= myMaxAcceleration;

	steering.myAngular = 0;

	return steering;;
}
