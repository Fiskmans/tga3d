//#include "pch.h"
#include "VelocityMatch.hpp"
#include "../SteeringOutput.hpp"
#include "../Kinematic.h"

AI::SteeringOutput AI::VelocityMatch::GetSteering()
{
	SteeringOutput steering;
	steering.myLinear = myTarget.myVelocity - myCharacter->myVelocity;
	steering.myLinear /= myTimeToTarget;

	if(steering.myLinear.Length() > myMaxAcceleration)
	{
		steering.myLinear.Normalize();
		steering.myLinear *= myMaxAcceleration;
	}

	steering.myAngular = 0;
	return steering;
}
