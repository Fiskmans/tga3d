#pragma once
#include "Seek.hpp"
namespace AI
{
	class Pursue :
		public Seek
	{
	public:
		Pursue(const Kinematic* aCharacter, const Kinematic* aTarget,
			const float aMaxAcceleration, const float aMaxPrediction)
			: Seek(aCharacter, &myInternalTarget, aMaxAcceleration),
			myMaxPrediction(aMaxPrediction), myInternalTargetPtr(aTarget), myInternalTarget(*aTarget)
		{
		}


		SteeringOutput GetSteering() override;
	protected:
		float myMaxPrediction;
		const Kinematic* myInternalTargetPtr;
		Kinematic myInternalTarget;
	};
}

