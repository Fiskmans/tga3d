#pragma once
#include "../Kinematic.h"
#include "../SteeringOutput.hpp"
#include "../SteeringBehaviour.hpp"

namespace AI
{
	class Arrive : public SteeringBehaviour
	{
	public:
		Arrive(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration, const float aMaxSpeed, const float aTargetRadius,
			const float aSlowRadius);

		SteeringOutput GetSteering() override;

	private:
		float myMaxAcceleration;
		float myMaxSpeed;
		float myTargetRadius;
		float mySlowRadius;
		float myTimeToTarget;
	};
}

