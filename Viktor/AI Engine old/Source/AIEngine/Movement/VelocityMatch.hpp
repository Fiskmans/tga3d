#pragma once
#include "../Kinematic.h"
#include "../SteeringOutput.hpp"
#include "../SteeringBehaviour.hpp"
namespace AI
{
	class VelocityMatch : public SteeringBehaviour
	{
	public:

		VelocityMatch(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration, const float aTimeToTarget)
			: SteeringBehaviour(aCharacter, aTarget),
			myMaxAcceleration(aMaxAcceleration),
			myTimeToTarget(aTimeToTarget)
		{
		}

		SteeringOutput GetSteering() override;

	private:
		float myMaxAcceleration{};

		float myTimeToTarget = 0.1f;
	};
}

