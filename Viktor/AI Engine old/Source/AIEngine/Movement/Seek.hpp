#pragma once
#include "../Kinematic.h"
#include "../SteeringOutput.hpp"
#include "../SteeringBehaviour.hpp"
namespace AI
{
	class Seek : public SteeringBehaviour
	{
	public:

		Seek(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration);

		SteeringOutput GetSteering() override;
	protected:
		float myMaxAcceleration;
	};
}

