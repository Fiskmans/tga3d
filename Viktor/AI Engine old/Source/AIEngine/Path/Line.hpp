#pragma once
#include "../Vector/Vector2.hpp"

namespace AI
{
	namespace CU
	{
		namespace Math
		{
			template <class T>
			class Line
			{
			public:
				Line() = default;
				Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);
				void InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);
				void InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection);
				bool Inside(const Vector2<T>& aPosition) const;
				T DistanceToPoint(const Vector2<T>& aPosition) const;

			private:
				Vector2<T> myPoint, myDirection;
			};

			template<class T>
			inline Line<T>::Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
				: myPoint(aPoint0)
				, myDirection((aPoint1 - aPoint0))
			{
			}

			template<class T>
			inline void Line<T>::InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
			{
				InitWithPointAndDirection(aPoint0, aPoint1 - aPoint0);
			}

			template<class T>
			inline void Line<T>::InitWithPointAndDirection(const Vector2<T> & aPoint, const Vector2<T> & aDirection)
			{
				myPoint = aPoint;
				myDirection = aDirection;
			}

			template<class T>
			inline bool Line<T>::Inside(const Vector2<T> & aPosition) const
			{
				return DistanceToPoint(aPosition) <= 0;
			}

			template<class T>
			inline T Line<T>::DistanceToPoint(const Vector2<T> & aPosition) const
			{
				return (aPosition - myPoint).Dot(myDirection.RightNormal());
			}

			typedef Line<int>	 Linei;
			typedef Line<float>  Linef;
			typedef Line<double> Lined;
		}

	}
}