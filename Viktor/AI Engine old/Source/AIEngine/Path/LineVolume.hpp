#pragma once
#include <vector>
#include "../Vector/Vector2.hpp"
#include "Line.hpp"
namespace AI
{
	namespace CU
	{
		namespace Math
		{
			template <class T>
			class LineVolume
			{
			public:
				LineVolume(const std::vector<Line<T>>& aLineList);
				void AddLine(const Line<T>& aLine);
				bool Inside(const Vector2<T>& aPosition);
				LineVolume() = default;
			private:
				std::vector<Line<T>> myLines;
			};

			template<class T>
			inline LineVolume<T>::LineVolume(const std::vector<Line<T>>& aLineList)
			{
				myLines = aLineList;
			}

			template<class T>
			inline void LineVolume<T>::AddLine(const Line<T>& aLine)
			{
				myLines.push_back(aLine);
			}

			template<class T>
			inline bool LineVolume<T>::Inside(const Vector2<T>& aPosition)
			{
				for (auto lines : myLines)
				{
					if (!lines.Inside(aPosition))
					{
						return false;
					}
				}
				return true;
			}

			typedef LineVolume<int>	   LineVolumei;
			typedef LineVolume<float>  LineVolumef;
			typedef LineVolume<double> LineVolumed;
		}
	}
}