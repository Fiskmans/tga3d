//#include "pch.h"
#include "Face.hpp"

AI::SteeringOutput AI::Face::GetSteering()
{
	myInternalTarget = *myInternalTargetPtr;
	const CU::Vector3f direction = myInternalTarget.myPosition - myCharacter->myPosition;

	if (direction.Length() == 0)
		return {};

	Align::myTargetPtr = myTargetPtr;

	//TODO: Change to direction.z instead of direction.y when 3D
	myInternalTarget.myOrientation = atan2(direction.x, -direction.y);
	
	return Align::GetSteering();
}
