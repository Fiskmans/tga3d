//#include "../pch.h"
#include "Align.hpp"
#include "../Tools.h"

AI::Align::Align(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAngularAcceleration,
	const float aMaxRotation, const float aTargetRdius, const float aSlowRadius)
	:SteeringBehaviour(aCharacter, aTarget),
	myMaxAngularAcceleration(aMaxAngularAcceleration),
	myMaxRotation(aMaxRotation),
	myTargetRdius(aTargetRdius),
	mySlowRadius(aSlowRadius),
	myTimeToTarget(0.1f)
{
	myTarget = *myTargetPtr;
}

AI::SteeringOutput AI::Align::GetSteering()
{
	SteeringOutput steering;

	float rotation = myTargetPtr->myOrientation - myCharacter->myOrientation;

	rotation = Tools::MapToRange(rotation);
	const float rotationSize = abs(rotation);

	//myTarget = *myTargetPtr;
	if (rotationSize < myTargetRdius)
		return{};


	float targetRotation;
	if(rotationSize > mySlowRadius)
		targetRotation = myMaxRotation;
	else
		targetRotation = myMaxRotation * rotationSize / mySlowRadius;
	

	targetRotation *= rotation / rotationSize;

	steering.myAngular = targetRotation - myCharacter->myRotation;
	steering.myAngular /= myTimeToTarget;

	const float angularAcc = abs(steering.myAngular);
	if(angularAcc > myMaxAngularAcceleration)
	{
		steering.myAngular /= angularAcc;
		steering.myAngular *= myMaxAngularAcceleration;
	}

	steering.myLinear = {0,0,0};

	return steering;
}
