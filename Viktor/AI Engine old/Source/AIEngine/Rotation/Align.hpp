#pragma once
#include "../Kinematic.h"
#include "../SteeringOutput.hpp"
#include "../SteeringBehaviour.hpp"
namespace AI
{
	class Align : public SteeringBehaviour
	{
	public:

		Align(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAngularAcceleration, const float aMaxRotation,
			const float aTargetRdius, const float aSlowRadius);

		SteeringOutput GetSteering() override;

	protected:

		float myMaxAngularAcceleration;
		float myMaxRotation;
		float myTargetRdius;
		float mySlowRadius;
		float myTimeToTarget;
	};
}

