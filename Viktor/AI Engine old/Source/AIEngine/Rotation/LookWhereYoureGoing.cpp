//#include "pch.h"
#include "LookWhereYoureGoing.hpp"

AI::SteeringOutput AI::LookWhereYoureGoing::GetSteering()
{
	if (myCharacter->myVelocity.Length() == 0)
		return {};

	myTarget.myOrientation = atan2(myCharacter->myVelocity.x, -myCharacter->myVelocity.y);
	return Align::GetSteering();
}
