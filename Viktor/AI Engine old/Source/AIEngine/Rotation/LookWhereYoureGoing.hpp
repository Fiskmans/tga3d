#pragma once
#include "Align.hpp"

namespace AI
{
	class LookWhereYoureGoing :
		public Align
	{
	public:
		LookWhereYoureGoing(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration,
			const float aMaxRotation = 3.f, const float aTargetRdius = 0.05f, const float aSlowRadius = 0.2f)
			: Align(aCharacter, aTarget, aMaxAcceleration, aMaxRotation, aTargetRdius, aSlowRadius)
		{
		}

	private:
		SteeringOutput GetSteering() override;
	};
}

