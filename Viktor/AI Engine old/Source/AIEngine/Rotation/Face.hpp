#pragma once
#include "Align.hpp"

namespace AI
{
	class Face :
		public Align
	{
	public:
		Face(const Kinematic* aCharacter, const Kinematic* aTarget, const float aMaxAcceleration, const float aMaxRotation,
			const float aTargetRdius = 0.2f, const float aSlowRadius = 0.5f)
			: Align(aCharacter, &myInternalTarget, aMaxAcceleration, aMaxRotation, aTargetRdius, aSlowRadius),
			myInternalTargetPtr(&myInternalTarget),
			myInternalTarget(*aTarget)
		{
		}

		SteeringOutput GetSteering() override;

	protected:
		const Kinematic* myInternalTargetPtr;
		Kinematic myInternalTarget;
	};
}

