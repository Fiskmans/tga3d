#pragma once
#include <iostream>
namespace AI
{
	namespace CU
	{
		namespace Math
		{
			template <class T>
			class Vector3
			{
			public:
				T x, y, z;

				//Creates a null-vector
				Vector3<T>();
				//Creates a vector (aX, aY, aZ)
				Vector3<T>(const T& aX, const T& aY, const T& aZ);
				//Copy constructor (compiler generated)
				Vector3<T>(const Vector3<T>& aVector) = default;
				//Assignment operator (compiler generated)
				Vector3<T>& operator=(const Vector3<T>& aVector3) = default;
				//Destructor (compiler generated)
				~Vector3<T>() = default;
				//Returns the squared length of the vector
				T LengthSqr() const;
				//Returns the length of the vector
				T Length() const;
				//Returns a normalized copy of this
				Vector3<T> GetNormalized() const;
				//Normalizes the vector
				void Normalize();
				//Returns the dot product of this and aVector
				T Dot(const Vector3<T>& aVector) const;
				//Returns the cross product of this and aVector
				Vector3<T> Cross(const Vector3<T>& aVector) const;

				Vector3<T> Reflect(const Vector3<T>& aNormal) const;
				Vector3<T> Refract(const Vector3<T>& aNormal, const T& anEta) const;

				void Print();
			};

			template <class T>
			Vector3<T>::Vector3()
				: x(0)
				, y(0)
				, z(0)
			{
			}

			template <class T>
			Vector3<T>::Vector3(const T& aX, const T& aY, const T& aZ)
				: x(aX)
				, y(aY)
				, z(aZ)
			{
			}

			template <class T>
			Vector3<T> Vector3<T>::GetNormalized() const
			{
				T inverse = T(1) / Length();
				return *this* inverse;
			}

			template <class T>
			void Vector3<T>::Normalize()
			{
				T inverse = T(1) / Length();
				*this *= inverse;
			}

			template <class T>
			T Vector3<T>::Dot(const Vector3<T>& aVector) const
			{
				return x * aVector.x + y * aVector.y + z * aVector.z;
			}

			template <class T>
			Vector3<T> Vector3<T>::Cross(const Vector3<T>& aVector) const
			{
				return Vector3(y * aVector.z - z * aVector.y,
					z * aVector.x - x * aVector.z,
					x * aVector.y - y * aVector.x);
			}

			// https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
			template <class T>
			Vector3<T> Vector3<T>::Reflect(const Vector3<T> & aNormal) const
			{
				return *this - T(2) * aNormal.Dot(*this) * aNormal;
			}

			// https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
			template <class T>
			Vector3<T> Vector3<T>::Refract(const Vector3<T> & aNormal, const T & anEta) const
			{
				T k = 1 - anEta * anEta * (1 - aNormal.Dot(*this) * aNormal.Dot(*this));
				if (k < 0.0)
					return Vector3();

				return anEta * *this - (anEta * aNormal.Dot(*this) + sqrt(k)) * aNormal;
			}

			template <class T>
			void Vector3<T>::Print()
			{
				std::cout << "vec3 = (" << x << ", " << y << ", " << z << ")" << std::endl;
			}

			template<class T>
			std::ostream& operator<<(std::ostream & os, const Vector3<T> & v)
			{
				return os << "V = (" << v.x << ", " << v.y << ", " << v.z << ")" << "\n";
			}


			template <class T>
			T Vector3<T>::LengthSqr() const
			{
				return Dot(*this);
			}

			template <class T>
			T Vector3<T>::Length() const
			{
				return std::sqrt(x * x + y * y + z * z);
			}

			//Returns the vector sum of aVector0 and aVector1
			template <class T>
			Vector3<T> operator+(const Vector3<T> & aVector0, const Vector3<T> & aVector1)
			{
				return Vector3<T>(aVector0.x + aVector1.x, aVector0.y + aVector1.y, aVector0.z + aVector1.z);
			}

			//Returns the vector difference of aVector0 and aVector1
			template <class T> Vector3<T> operator-(const Vector3<T> & aVector0, const Vector3<T> & aVector1)
			{
				return Vector3<T>(aVector0.x - aVector1.x, aVector0.y - aVector1.y, aVector0.z - aVector1.z);
			}
			//Returns the vector aVector multiplied by the scalar aScalar
			template <class T> Vector3<T> operator*(const Vector3<T> & aVector, const T & aScalar)
			{
				return Vector3<T>(aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar);
			}
			//Returns the vector aVector multiplied by the scalar aScalar
			template <class T> Vector3<T> operator*(const T & aScalar, const Vector3<T> & aVector)
			{
				return Vector3<T>(aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar);
			}
			//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
			template <class T> Vector3<T> operator/(const Vector3<T> & aVector, const T & aScalar)
			{
				return Vector3<T>(aVector.x / aScalar, aVector.y / aScalar, aVector.z / aScalar);
			}

			template <class T> Vector3<float> operator/(const Vector3<float> & aVector, const float& aScalar)
			{
				const float fact = 1 / aScalar;
				return Vector3<float>(aVector.x * fact, aVector.y * fact, aVector.z * fact);
			}

			template <class T> Vector3<double> operator/(const Vector3<double> & aVector, const double& aScalar)
			{
				const double fact = 1 / aScalar;
				return Vector3<double>(aVector.x * fact, aVector.y * fact, aVector.z * fact);
			}

			//Equivalent to setting aVector0 to (aVector0 + aVector1)
			template <class T> void operator+=(Vector3<T> & aVector0, const Vector3<T> & aVector1)
			{
				aVector0 = aVector0 + aVector1;
			}
			//Equivalent to setting aVector0 to (aVector0 - aVector1)
			template <class T> void operator-=(Vector3<T> & aVector0, const Vector3<T> & aVector1)
			{
				aVector0 = aVector0 - aVector1;
			}
			//Equivalent to setting aVector to (aVector * aScalar)
			template <class T> void operator*=(Vector3<T> & aVector, const T & aScalar)
			{
				aVector = aVector * aScalar;
			}
			//Equivalent to setting aVector to (aVector / aScalar)
			template <class T> void operator/=(Vector3<T> & aVector, const T & aScalar)
			{
				aVector = aVector / aScalar;
			}
		}

		using namespace Math;

		typedef Vector3<int>	Vector3i;
		typedef Vector3<float>	Vector3f;
		typedef Vector3<double> Vector3d;
	}
}