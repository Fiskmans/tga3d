#pragma once
#include <cmath>
#include <iostream>

namespace AI
{
	namespace CU
	{
		namespace Math
		{
			template <class T>
			class Vector2
			{
			public:
				T x, y;

				static Vector2<T> One;
				static Vector2<T> Zero;
				static Vector2<T> UnitX;
				static Vector2<T> UnitY;

				//Creates a null-vector
				Vector2<T>();
				//Creates a vector (aX, aY)
				Vector2<T>(const T& aX, const T& aY);
				//Copy constructor (compiler generated)
				Vector2<T>(const Vector2<T>& aVector) = default;
				//Assignment operator (compiler generated)
				Vector2<T>& operator=(const Vector2<T>& aVector2) = default;
				//Destructor (compiler generated)
				~Vector2<T>() = default;
				//Returns the squared length of the vector
				T LengthSqr() const;
				//Returns the length of the vector
				T Length() const;
				//Returns a normalized copy of this
				Vector2<T> GetNormalized() const;
				//Normalizes the vector
				void Normalize();
				// Right Normal
				Vector2<T> RightNormal() const;
				// Left Normal
				Vector2<T> LeftNormal() const;
				//Returns the dot product of this and aVector
				T Dot(const Vector2<T>& aVector) const;
				T Wedge(const Vector2<T>& aVector) const;

				Vector2<T> Reflect(const Vector2<T>& aNormal) const;
				Vector2<T> Refract(const Vector2<T>& aNormal, const T& anEta) const;

				Vector2<T>& Rotate(float anAngle);
				void RotateAround(float anAngle, const Vector2<T>& aAnchor);

				void Print();
			};

			template <class T>
			Vector2<T>::Vector2()
				: x(0)
				, y(0)
			{
			}

			template <class T>
			Vector2<T>::Vector2(const T& aX, const T& aY)
				: x(aX)
				, y(aY)
			{
			}

			template <class T>
			T Vector2<T>::LengthSqr() const
			{
				return Dot(*this);
			}

			template <class T>
			T Vector2<T>::Length() const
			{
				return sqrt(x * x + y * y);
			}

			template <class T>
			Vector2<T> Vector2<T>::GetNormalized() const
			{
				T inverse = T(1) / Length();
				return *this* inverse;
			}

			template <class T>
			void Vector2<T>::Normalize()
			{
				T inverse = T(1) / Length();
				*this *= inverse;
			}


			template<class T>
			inline Vector2<T> Vector2<T>::RightNormal() const
			{
				return Vector2<T>(-y, x);
			}

			template<class T>
			inline Vector2<T> Vector2<T>::LeftNormal() const
			{
				return Vector2<T>(y, -x);
			}

			template <class T>
			T Vector2<T>::Dot(const Vector2<T> & aVector) const
			{
				return x * aVector.x + y * aVector.y;
			}

			template<class T>
			inline T Vector2<T>::Wedge(const Vector2<T> & aVector) const
			{
				return x * aVector.y - y * aVector.x;
			}

			// https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/reflect.xhtml
			template <class T>
			Vector2<T> Vector2<T>::Reflect(const Vector2<T> & aNormal) const
			{
				return *this - T(2) * aNormal.Dot(*this) * aNormal;
			}

			// https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/refract.xhtml
			template <class T>
			Vector2<T> Vector2<T>::Refract(const Vector2<T> & aNormal, const T & anEta) const
			{
				T k = 1 - anEta * anEta * (1 - aNormal.Dot(*this) * aNormal.Dot(*this));
				if (k < 0.0)
					return Vector2();

				return anEta * *this - (anEta * aNormal.Dot(*this) + sqrt(k)) * aNormal;
			}

			template <class T>
			Vector2<T> & Vector2<T>::Rotate(const float anAngle)
			{
				float c = cosf(anAngle);
				float s = sinf(anAngle);
				T xTemp = x;
				x = x * c - y * s;
				y = y * c + xTemp * s;
				return *this;
			}

			template <class T>
			void Vector2<T>::RotateAround(const float anAngle, const Vector2<T> & aAnchor)
			{
				*this -= aAnchor;
				Rotate(anAngle);
				*this += aAnchor;
			}

			template <class T>
			void Vector2<T>::Print()
			{
				std::cout << "vec2 = (" << x << ", " << y << ")" << std::endl;
			}

			template<class T>
			std::ostream& operator<<(std::ostream & os, const Vector2<T> & v)
			{
				return os << "V = (" << v.x << ", " << v.y << ")" << "\n";
			}

			//Returns the vector sum of aVector0 and aVector1
			template <class T>
			Vector2<T> operator+(const Vector2<T> & aVector0, const Vector2<T> & aVector1)
			{
				return Vector2<T>(aVector0.x + aVector1.x, aVector0.y + aVector1.y);
			}

			//Returns the vector difference of aVector0 and aVector1
			template <class T> Vector2<T> operator-(const Vector2<T> & aVector0, const Vector2<T> & aVector1)
			{
				return Vector2<T>(aVector0.x - aVector1.x, aVector0.y - aVector1.y);
			}
			//Returns the vector aVector multiplied by the scalar aScalar
			template <class T> Vector2<T> operator*(const Vector2<T> & aVector, const T & aScalar)
			{
				return Vector2<T>(aVector.x * aScalar, aVector.y * aScalar);
			}
			//Returns the vector aVector multiplied by the scalar aScalar
			template <class T> Vector2<T> operator*(const T & aScalar, const Vector2<T> & aVector)
			{
				return Vector2<T>(aVector.x * aScalar, aVector.y * aScalar);
			}
			//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1 / aScalar)
			template <class T> Vector2<T> operator/(const Vector2<T> & aVector, const T & aScalar)
			{
				return Vector2<T>(aVector.x / aScalar, aVector.y / aScalar);
			}

			template <class T> Vector2<float> operator/(const Vector2<float> & aVector, const float& aScalar)
			{
				const float fact = 1.0f / aScalar;
				return Vector2<float>(aVector.x * fact, aVector.y * fact);
			}

			template <class T> Vector2<double> operator/(const Vector2<double> & aVector, const double& aScalar)
			{
				const double fact = 1.0 / aScalar;
				return Vector2<double>(aVector.x * fact, aVector.y * fact);
			}

			//Equivalent to setting aVector0 to (aVector0 + aVector1)
			template <class T> void operator+=(Vector2<T> & aVector0, const Vector2<T> & aVector1)
			{
				aVector0 = aVector0 + aVector1;
			}
			//Equivalent to setting aVector0 to (aVector0 - aVector1)
			template <class T>
			void operator-=(Vector2<T> & aVector0, const Vector2<T> & aVector1)
			{
				aVector0 = aVector0 - aVector1;
			}
			//Equivalent to setting aVector to (aVector * aScalar)
			template <class T> void operator*=(Vector2<T> & aVector, const T & aScalar)
			{
				aVector = aVector * aScalar;
			}
			//Equivalent to setting aVector to (aVector / aScalar)
			template <class T> void operator/=(Vector2<T> & aVector, const T & aScalar)
			{
				aVector = aVector / aScalar;
			}

			template <class T> Vector2<T> Vector2<T>::Zero = { 0, 0 };
			template <class T> Vector2<T> Vector2<T>::UnitX = { 1, 0 };
			template <class T> Vector2<T> Vector2<T>::UnitY = { 0, 1 };
			template <class T> Vector2<T> Vector2<T>::One = { 1, 1 };
		}

		using namespace Math;

		typedef Vector2<int>	Vector2i;
		typedef Vector2<float>	Vector2f;
		typedef Vector2<double> Vector2d;
	}
}