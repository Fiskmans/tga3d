#pragma once
#include "Vector/Vector3.hpp"

namespace AI
{
	class SteeringOutput
	{
	public:
		SteeringOutput() = default;
		~SteeringOutput() = default;

		AI::CU::Vector3f myLinear;
		float myAngular{};
	};
}

