#pragma once
#include "Vector/Vector3.hpp"
namespace AI
{
	class SteeringOutput;

	struct Static
	{
		AI::CU::Vector3f myPosition;
		float myOrientation{};
	};

	/*struct KinematicSteeringOutput
	{
		AI::CU::Vector3f myVelocity;
		float myRotation{};
	};*/

	class Kinematic
	{
	public:
		Kinematic() = default;
		~Kinematic() = default;

		void Update(const SteeringOutput& aSteering, float aMaxSpeed, float aDeltaTime);

		static float GetNewOrientation(const float& aCurrentOrientation, const AI::CU::Vector3f& aVelocity);

		AI::CU::Vector3f myPosition;
		float myOrientation{};
		AI::CU::Vector3f myVelocity;
		float myRotation{};
	};

	/*class KinematicSeek
	{
	public:
		KinematicSeek(const Static& aCharacter, const Static& aTarget, float aMaxSpeed)
			: myCharacter(aCharacter),
			myTarget(aTarget), myMaxSpeed(aMaxSpeed)
		{
		}

		KinematicSteeringOutput GetSteering();

	private:
		Static myCharacter;
		Static myTarget;
		float myMaxSpeed;
	};

	class KinematicArrive
	{
	public:
		KinematicArrive(const Static& aCharacter, const Static& aTarget, float aMaxSpeed)
			: myCharacter(aCharacter),
			myTarget(aTarget),
			myMaxSpeed(aMaxSpeed), myRadius(0), myTimeToTarget(0.25f)
		{
		}

		KinematicSteeringOutput GetSteering();

	private:
		Static myCharacter;
		Static myTarget;
		float myMaxSpeed{};
		float myRadius;
		const float myTimeToTarget;

	};

	class KinematicWanderer
	{
	public:
		KinematicWanderer(const Static& aCharacter, float aMaxSpeed, float aMaxRotation)
			: myCharacter(aCharacter),
			myMaxSpeed(aMaxSpeed),
			myMaxRotation(aMaxRotation)
		{
		}

		KinematicSteeringOutput GetSteering() const;

	private:
		Static myCharacter;
		float myMaxSpeed{};
		float myMaxRotation{};
	};*/
}