#include "pch.h"
#include "Kinematic.h"
#include "SteeringOutput.hpp"
#include "Tools.h"



/**
 * \brief 
 * \param aSteering A steering output given from any class derived from SteeringBehavior
 * \param aMaxSpeed A max speed that the character can move
 * \param aDeltaTime 
 */
void AI::Kinematic::Update(const SteeringOutput& aSteering, const float aMaxSpeed, const float aDeltaTime)
{
	myPosition += myVelocity * aDeltaTime;
	myOrientation += myRotation * aDeltaTime;

	myVelocity += aSteering.myLinear * aDeltaTime;
	myOrientation += aSteering.myAngular * aDeltaTime;

	if(myVelocity.Length() > aMaxSpeed)
	{
		myVelocity.Normalize();
		myVelocity *= aMaxSpeed;
	}
}

float AI::Kinematic::GetNewOrientation(const float& aCurrentOrientation, const AI::CU::Vector3f& aVelocity)
{
	if (aVelocity.Length() > 0)
	{
		return atan2(-aVelocity.x, aVelocity.z);
	}
	return aCurrentOrientation;
}

/* AI::KinematicSteeringOutput AI::KinematicSeek::GetSteering()
{
	KinematicSteeringOutput steering;

	steering.myVelocity = myTarget.myPosition - myCharacter.myPosition;

	steering.myVelocity.Normalize();
	steering.myVelocity *= myMaxSpeed;

	myCharacter.myOrientation = Kinematic::GetNewOrientation(myCharacter.myOrientation, steering.myVelocity);

	steering.myRotation = 0;
	return steering;
}

AI::KinematicSteeringOutput AI::KinematicArrive::GetSteering()
{
	KinematicSteeringOutput steering;
	steering.myVelocity = myTarget.myPosition - myCharacter.myPosition;

	if(steering.myVelocity.Length() < myRadius)
	{
		return {};
	}

	steering.myVelocity /= myTimeToTarget;


	if(steering.myVelocity.Length() > myMaxSpeed)
	{
		steering.myVelocity.Normalize();
		steering.myVelocity *= myMaxSpeed;
	}

	myCharacter.myOrientation = Kinematic::GetNewOrientation(myCharacter.myOrientation, steering.myVelocity);

	steering.myRotation = 0;
	return steering;
}

AI::KinematicSteeringOutput AI::KinematicWanderer::GetSteering() const
{
	KinematicSteeringOutput steering;

	steering.myVelocity = myMaxSpeed * Tools::FloatToVector3(myCharacter.myOrientation);

	steering.myRotation = Tools::RandomBinomial() * myMaxRotation;

	return steering;
}*/
