#include "SteeringBehaviour.hpp"

AI::SteeringBehaviour::SteeringBehaviour(const Kinematic* aCharacter, const Kinematic* aTarget)
	: myCharacter(aCharacter), myTargetPtr(aTarget)
{
	myTarget = *myTargetPtr;
}