#pragma once
#include "Kinematic.h"
namespace AI
{
	class SteeringBehaviour
	{
	public:
		SteeringBehaviour() = default;
		virtual ~SteeringBehaviour() = default;
		SteeringBehaviour(const Kinematic* aCharacter, const Kinematic* aTarget);
		virtual SteeringOutput GetSteering() = 0;
	protected:
		const Kinematic* myCharacter{};
		const Kinematic* myTargetPtr{};

		Kinematic myTarget;
	};
}

