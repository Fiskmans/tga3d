#include "pch.h"
#include "Tools.h"
#include <random>
#include <math.h>

AI::CU::Vector2f Tools::FloatToVector2(const float& aFloat)
{
	return { sin(aFloat), cos(aFloat) };
}

AI::CU::Vector3f Tools::FloatToVector3(const float& aFloat)
{
	return { sin(aFloat), -cos(aFloat), 0 };
}

float Tools::RandomBinomial()
{
	// TODO: Change this into a properly working version
	static std::default_random_engine  rd;
	static std::mt19937 e2(rd());

	static std::bernoulli_distribution dist(0.05);

	const float r = dist(rd);
	const float r2 = dist(e2);
	return r - r2;
}

/*
 * Normalizes the float value to be in a range of -pi to pi
 */
float Tools::MapToRange(const float aFloat)
{
	long double x = fmod(aFloat + std::_Pi, 2.f * std::_Pi);
	if (x < 0)
		x += 2.f * std::_Pi;
	return static_cast<float>(x - std::_Pi);
}
