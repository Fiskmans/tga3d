#include "stdafx.h"
#include "StateMachine.hpp"
#include <assert.h>

StateMachine::StateMachine()
{

}

StateMachine::~StateMachine()
{

}

void StateMachine::Start(State * aStartState)
{
	myCurrentState = aStartState;
}

void StateMachine::SwitchState(State* aOtherState)
{
	myCurrentState->OnExit();
	myCurrentState = aOtherState;
	myCurrentState->OnEnter();
}

void StateMachine::Update(float aDeltaTime)
{
	myCurrentState->Update(aDeltaTime);
}

const State * StateMachine::GetCurrentStatePtr() const
{
	return myCurrentState;
}
