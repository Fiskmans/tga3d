#include "stdafx.h"
#include "State.hpp"
#include <iostream>
#include "StateMachine.hpp"

State::State()
{
	myStateMachine = nullptr;
}

State::State(StateMachine * aStateMachine)
{
	myStateMachine = aStateMachine;
}

State::~State()
{
}

void State::OnEnter()
{
}

void State::OnExit()
{
}

void State::Update(float /*aDeltaTime*/)
{

}

void State::SwitchState()
{
	for (auto i : aStateMap)
	{
		if (i.second())
		{
			myStateMachine->SwitchState(i.first);
		}
	}
}

void State::SetCondition(std::function<bool()> aCondition, State* aOtherState)
{
	aStateMap[aOtherState] = aCondition;
}
