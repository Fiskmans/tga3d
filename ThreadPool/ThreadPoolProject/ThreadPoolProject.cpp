// ThreadPoolProject.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#pragma once
#include "pch.h"
#include <iostream>
#include "ThreadPool.hpp"
#include <atomic>
#include "ThreadPoolProject.h"
#include <vector>
#include <array>
#include "TestClass.hpp"

void print_num(int i)
{
	std::cout << i << '\n';
}
int main()
{
	TestClass test;
	test.SetText("lololo");
	//int add = 0;
	std::function<void(int)> ko = print_num;
	ThreadPool::Create();
	//test.Print();
	Work work;
	work.SetWork(ko, 9);
	ThreadPool::GetInstance()->GiveWork(work);
	ThreadPool::Destroy();
	while (true)
	{

	}
	//return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
