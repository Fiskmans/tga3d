#include "pch.h"
#include "TestClass.hpp"
#include "ThreadPool.hpp"

TestClass::TestClass()
{
}


TestClass::~TestClass()
{
}

void TestClass::Print()
{
	Work work;
	std::function<void(std::string)> lol = std::bind(&TestClass::InternalPrint, this, std::placeholders::_1);
	//work.SetWorkWithParameters<std::string>(std::move(lol), print);
	ThreadPool::GetInstance()->GiveWork(work);
}

void TestClass::SetText(std::string aString)
{
	print = aString;
}

void TestClass::InternalPrint(std::string aString)
{
	std::cout << aString;
}
