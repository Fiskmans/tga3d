#pragma once
#include <iostream>
#include <string>

class TestClass
{
public:
	TestClass();
	~TestClass();
	void Print();
	void SetText(std::string aString);
	void InternalPrint(std::string aString);
private:
	std::string print;
};

