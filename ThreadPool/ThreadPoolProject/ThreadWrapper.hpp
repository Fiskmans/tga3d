#pragma once
#include <thread>
class ThreadWrapper
{
public:
	ThreadWrapper();
	~ThreadWrapper();
private:
	void Run();
	bool myIsRunning = true;
	
};

